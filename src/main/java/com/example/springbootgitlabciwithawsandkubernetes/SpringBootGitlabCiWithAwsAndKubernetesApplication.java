package com.example.springbootgitlabciwithawsandkubernetes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootGitlabCiWithAwsAndKubernetesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootGitlabCiWithAwsAndKubernetesApplication.class, args);
    }

}
